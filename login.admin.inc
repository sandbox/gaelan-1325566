<?php
	function activegrade_intergration_login(){
		$form["root"] = array(
									   "#title" => t("URL"),
									   "#type" => "textfield",
									   "#description" => t("The URL you use to log into ActiveGrade. You can leave blank unless you use a school ActiveGrade account."),
									   "#default_value" => variable_get("root", ""),
									   "#field_prefix" => "http://activegrade.appspot.com/",
									   "#weight" => -30,
									   );
		$form["username"] = array(
										   "#title" => t("Email"),
										   "#type" => "textfield",
										   "#description" => t("The email you use to log into ActiveGrade."),
										   "#default_value" => variable_get("username", ""),
										   "#required" => TRUE,
										   "#weight" => -20,
										   );
		$form["password"] = array(
								  //@todo is this a safe way to handle passwords?
										   "#title" => t("Password"),
										   "#type" => "password",
										   "#description" => t("The password you use to log into ActiveGrade."),
										   "#weight" => -10,
										   );
		return system_settings_form($form);
	}
	function activegrade_intergration_login_validate(&$form_state){
		$json=json_encode(array("/".$form_state["root"]['#value'], $form_state["username"]['#value'], $form_state["password"]['#value']));
		$result = drupal_http_request("https://activegrade.appspot.com/rest/verify", array('content-type' => "application/json"), 'POST', $json);
	    $response = json_decode($result->data);
		$response = $response[0];
		if (!empty($response)){
			variable_set('userid', $response->id);
			drupal_set_message(t("Hi @username, your user id is %id.", array(
								 "@username" => $response->displayName ? $response->displayName : $response->primaryEmailAddress, 
								 "%id" => $response->id)));
		}
		else{
			form_set_error("password", t("Your ActiveGrade login info is incorrect. Please try again."));
		}
	}
?>